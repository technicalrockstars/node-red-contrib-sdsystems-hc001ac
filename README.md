# node-red-contrib-sdsystems-hc001ac Node

## Installation

To install the stable version use the Menu - Manage palette - Install option and search for node-red-contrib-sdsystems-hc001ac, or run the following command in your Node-RED user directory, typically ~/.node-red

```
cd ~/.node-red
npm install node-red-contrib-sdsystems-hc001ac
```

## Usage

### Input

#### msg object

For this node, the input msg object is just a trigger.

#### sensor data

This node reads sensor data from a local serial port.
A user must set the serial port name (e.q. '/dev/ttyUSB0' ).

### Output

This node outputs msg objects including sensor data.
The format of the message is below.

```
"msg" : {
    "payload" : {
        "count" : count
        "elements" : [01, 03, ...]
    }
}
```

## Copyright and license

Copyright Uhuru Coproration under the Apache 2.0 license.