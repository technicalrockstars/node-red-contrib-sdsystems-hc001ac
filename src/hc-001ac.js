const SerialPort = require('serialport');

const NODE_STATUS_RUNNING = 1;                  // 通常動作中（初期化処理完了）
const NODE_STATUS_REQ_GET_INIT_IMG = 2;         // 初期環境画像取得処理中
const NODE_STATUS_RES_GET_INIT_IMG = 3;         // 初期環境画像取得処理中
const NODE_STATUS_REQ_MODE_ENV_DIFF = 4;        // 初期環境差分モードON/OFF設定中
const NODE_STATUS_RES_MODE_ENV_DIFF = 5;        // 初期環境差分モードON/OFF設定中
const NODE_STATUS_REQ_MODE_AUTO_ENV_UPDATE = 6; // 環境差分自動取得モードON/OFF設定中
const NODE_STATUS_RES_MODE_AUTO_ENV_UPDATE = 7; // 環境差分自動取得モードON/OFF設定中
const NODE_STATUS_REQ_HUMAN_THRESHOLD = 8;      // 人体閾値設定中
const NODE_STATUS_RES_HUMAN_THRESHOLD = 9;      // 人体閾値設定中
const NODE_STATUS_REQ_ENV_THRESHOLD = 10;       // 環境差分閾値設定中
const NODE_STATUS_RES_ENV_THRESHOLD = 11;       // 環境差分閾値設定中
const NODE_STATUS_REQ_STATUS = 12;              // ステータス要求中
const NODE_STATUS_RES_STATUS = 13;              // ステータス要求中
const NODE_STATUS_ERROR = 14;                   // エラー状態（シリアルポートオープン失敗)

const reqInit = [0x44];                 // 初期化
const reqModeEnvDiff = [0x41];          // 初期環境差分 ON/OFF
const reqModeAutoEnvUpdate = [0x4b];    // 環境差分自動取得 ON/OFF
const reqHumanThreshold = [0x58];       // 人体閾値設定
const reqEnvThreshold = [0x59];         // 初期環境差分閾値設定
const reqHumanCount = [0x72];           // 人体データ要求
const reqGetInitImg = [0x49];           // 初期環境画像取得要求
const reqStatus = [0x53];               // ステータス要求

module.exports = function(RED) {
    function HC001AC(config) {
        RED.nodes.createNode(this,config);
        this.port = config.port;
        this.reqInitImg = config.init_image_req;
        this.modeEnvDiff = config.mode_init_image;
        this.modeAutoEnvUpdate = config.mode_auto_env_update;
        this.humanThreshold = config.human_threshold;
        this.envThreshold = config.env_threshold;

        this.nodeStatus = NODE_STATUS_REQ_STATUS;
        let node = this;
        node.settings = {};

        let res = new Buffer.alloc(256);    // メッセージ受信用バッファ
        let reslen = 0;                     // 受信メッセージ長
        res.fill(0);

        /**
         * シリアルポートのオープン処理
         */
        const port = new SerialPort(node.port, {
            baudRate: 9600,
            dataBits: 8,
            parity: 'none',
            stopBits: 1,
            flowControl: false
        }, function(err) {
            if (err) {
                node.nodeStatus = NODE_STATUS_ERROR;
                node.error('Serial port open error : ', err.message);
                node.status({fill:"red",shape:"dot",text:"HC-001AC.status.disconnected"});
                return;
            }
            node.log('Serial port is opened.');
            node.status({fill:"blue",shape:"dot",text:"HC-001AC.status.connected"});
            port.write(reqStatus, function(err) {
                if (err) {
                    node.nodeStatus = NODE_STATUS_ERROR;
                    node.status({fill:"red",shape:"dot",text:"HC-001AC.status.disconnected"});
                    node.error('Serial port open error : ', err.message);
                    return;
                }
                node.nodeStatus = NODE_STATUS_RES_STATUS;
            });
        });

        /**
         * シリアルポートのデータ受信処理
         */
        port.on('data', function(data) {
            res.set(data, reslen);
            reslen = reslen + data.length;
            if ((reslen >= 256) || (res[reslen - 1] === 0x0A)) {
                let msg = {};
                let send = function() { node.send.apply(node, arguments) };
                if (node.nodeStatus == NODE_STATUS_RUNNING) {
                    msg.payload = decodeHumanCount(res.slice(0, reslen));
                    send(msg);
                } 
                else if (node.nodeStatus != NODE_STATUS_ERROR) {
                    while(true) {
                        initialize(port, node, res.slice(0, reslen));
                        if ((node.nodeStatus != NODE_STATUS_REQ_MODE_ENV_DIFF) &&
                            (node.nodeStatus != NODE_STATUS_REQ_MODE_AUTO_ENV_UPDATE) &&
                            (node.nodeStatus != NODE_STATUS_REQ_HUMAN_THRESHOLD) &&
                            (node.nodeStatus != NODE_STATUS_REQ_ENV_THRESHOLD) &&
                            (node.nodeStatus != NODE_STATUS_REQ_GET_INIT_IMG)) {
                            break;
                        }
                    }
                }
                reslen = 0;
                res.fill(0);    
            }
        });

        /**
         * シリアル通信エラー（例外）処理
         */
        port.on('error', function(err) {
            node.nodeStatus = NODE_STATUS_ERROR;
            node.status({fill:"red",shape:"dot",text:"HC-001AC.status.disconnected"});
            node.error('Serial port write error : ', err.message);
        });

        /**
         * ノードのmsgオブジェクト受信処理
         */
        node.on('input', function(msg, send, done) {
            if (node.nodeStatus === NODE_STATUS_RUNNING) {
                port.write(reqHumanCount, function(err) {
                    reslen = 0;
                    res.fill(0);
                    if (err) {
                        node.error('Serial port write error : ', err.message);
                        if (done) {
                            done();
                        }
                        return;
                    }
                });
            }
            else if (node.nodeStatus === NODE_STATUS_ERROR) {
                let send = function() { node.send.apply(node, arguments) };
                msg.payload = { 'humanCount': 'serial port error' };
                send(msg);
            }
            if (done) {
                done();
            }
        });

        /**
         * シリアルポートのクローズ処理
         */
        node.on('close', function() {
            if (port.isOpen) {
                port.close();
                node.log('Serial port is closed.');
            }
        });
    }

    RED.nodes.registerType("HC-001AC", HC001AC);
}

/**
 * sensorSetting()
 * 
 * @param {SerialPort} port 
 * @param {Node} node 
 * @param {Buffer} response 
 * @returns {Boolean}
 */
const initialize = function sensorSetting(port, node, response) {
    switch(node.nodeStatus) {
        case NODE_STATUS_RES_STATUS:           // ステータス要求
            decodeStatus(response, node.settings);
            node.nodeStatus = NODE_STATUS_REQ_MODE_ENV_DIFF;
            break;
        case NODE_STATUS_REQ_MODE_ENV_DIFF:        // 初期環境差分モードON/OFF設定中
            if (node.modeEnvDiff != node.settings.modeEnvDiff) {
                node.debug("初期環境差分モードON/OFF設定");
                if (!portWrite(port, reqModeEnvDiff, node)) {
                    node.nodeStatus = NODE_STATUS_RUNNING;
                    return false;
                }
                node.nodeStatus = NODE_STATUS_RES_MODE_ENV_DIFF;
            }
            else {
                node.nodeStatus = NODE_STATUS_REQ_MODE_AUTO_ENV_UPDATE;
            }
            break;
        case NODE_STATUS_RES_MODE_ENV_DIFF:        // 初期環境差分モードON/OFF設定中
            result = b2a(response);
            if ((result === 'OK') || (result === 'NO') || (result === 'NG')) {
                node.nodeStatus = NODE_STATUS_REQ_MODE_AUTO_ENV_UPDATE;
                if ((result === 'NO') || (result === 'NG')) {
                    node.error("初期環境差分モード設定エラー " + result);
                }
            }
            else {
                let value = node.modeEnvDiff ? [0x31] : [0x30];
                if (!portWrite(port, value, node)) {
                    node.nodeStatus = NODE_STATUS_RUNNING;
                    return false;
                }
            }
            break;
        case NODE_STATUS_REQ_MODE_AUTO_ENV_UPDATE: // 環境差分自動取得モードON/OFF設定中
            if (node.modeAutoEnvUpdate != node.settings.modeAutoEnvUpdate) {
                node.debug("環境差分自動取得モードON/OFF設定");
                if (!portWrite(port, reqModeAutoEnvUpdate, node)) {
                    node.nodeStatus = NODE_STATUS_RUNNING;
                    return false;
                }
                node.nodeStatus = NODE_STATUS_RES_MODE_AUTO_ENV_UPDATE;
            }
            else {
                node.nodeStatus = NODE_STATUS_REQ_HUMAN_THRESHOLD;
            }
            break;
        case NODE_STATUS_RES_MODE_AUTO_ENV_UPDATE: // 環境差分自動取得モードON/OFF設定中
            result = b2a(response);
            if ((result === 'OK') || (result === 'NO') || (result === 'NG')) {
                node.nodeStatus = NODE_STATUS_REQ_HUMAN_THRESHOLD;
                if ((result === 'NO') || (result === 'NG')) {
                    node.error("環境差分自動取得モード設定エラー " + result);
                }
            }
            else {
                let value = node.modeAutoEnvUpdate ? [0x31] : [0x30];
                if (!portWrite(port, value, node)) {
                    node.nodeStatus = NODE_STATUS_RUNNING;
                    return false;
                }
            }
            break;
        case NODE_STATUS_REQ_HUMAN_THRESHOLD:       // 人体閾値設定中
            if (parseInt(node.humanThreshold, 10) != node.settings.humanThreshold) {
                node.debug("人体閾値設定");
                if (!portWrite(port, reqHumanThreshold, node)) {
                    node.nodeStatus = NODE_STATUS_RUNNING;
                    return false;
                }
                node.nodeStatus = NODE_STATUS_RES_HUMAN_THRESHOLD;
            }
            else {
                node.nodeStatus = NODE_STATUS_REQ_ENV_THRESHOLD;
            }
            break;
        case NODE_STATUS_RES_HUMAN_THRESHOLD:       // 人体閾値設定中
            result = b2a(response);
            if ((result === 'OK') || (result === 'NO') || (result === 'NG')) {
                node.nodeStatus = NODE_STATUS_REQ_ENV_THRESHOLD;
                if ((result === 'NO') || (result === 'NG')) {
                    node.error("人体閾値設定エラー " + result);
                }
            }
            else {
                let value = new Uint8Array(2);
                value[1] = [0x30 + (parseInt(node.humanThreshold, 10) % 10)];
                value[0] = [0x30 + ((parseInt(node.humanThreshold, 10) / 10) % 10)];
                if (!portWrite(port, value, node)) {
                    node.nodeStatus = NODE_STATUS_RUNNING;
                    return false;
                }
            }
            break;
        case NODE_STATUS_REQ_ENV_THRESHOLD:        // 環境差分閾値設定中
            if (parseInt(node.envThreshold, 10) != node.settings.envThreshold) {
                node.debug("環境差分閾値設定");
                if (!portWrite(port, reqEnvThreshold, node)) {
                    node.nodeStatus = NODE_STATUS_RUNNING;
                    return false;
                }
                node.nodeStatus = NODE_STATUS_RES_ENV_THRESHOLD;
            }
            else {
                node.nodeStatus = NODE_STATUS_REQ_GET_INIT_IMG;
            }
            break;
        case NODE_STATUS_RES_ENV_THRESHOLD:        // 環境差分閾値設定中
            result = b2a(response);
            if ((result === 'OK') || (result === 'NO') || (result === 'NG')) {
                node.nodeStatus = NODE_STATUS_REQ_GET_INIT_IMG;
                if ((result === 'NO') || (result === 'NG')) {
                    node.error("環境差分閾値設定エラー " + result);
                }
            }
            else {
                let value = new Uint8Array(2);
                value[1] = [0x30 + (parseInt(node.envThreshold, 10) % 10)];
                value[0] = [0x30 + ((parseInt(node.envThreshold, 10) / 10) % 10)];
                if (!portWrite(port, value, node)) {
                    node.nodeStatus = NODE_STATUS_RUNNING;
                    return false;
                }
            }
            break;
        case NODE_STATUS_REQ_GET_INIT_IMG:         // 初期環境画像取得処理中
            if (node.reqInitImg) {
                node.debug("初期環境画像取得処理設定");
                if (!portWrite(port, reqGetInitImg, node)) {
                    node.nodeStatus = NODE_STATUS_RUNNING;
                    return false;
                }
                node.nodeStatus = NODE_STATUS_RES_GET_INIT_IMG;
            }
            else {
                node.nodeStatus = NODE_STATUS_RUNNING;
            }
            break;
        case NODE_STATUS_RES_GET_INIT_IMG:         // 初期環境画像取得処理中
            result = b2a(response);
            if ((result === 'OK') || (result === 'NO') || (result === 'NG')) {
                node.nodeStatus = NODE_STATUS_RUNNING;
                if ((result === 'NO') || (result === 'NG')) {
                    node.error("初期環境画像取得設定エラー " + result);
                }
           }
            else {
                node.nodeStatus = NODE_STATUS_RUNNING;
            }
            break;
    }
    return true;
}

/**
 * portWriteMessage() send a message to the serial port.
 * 
 * @param {SerialPort} port 
 * @param {Buffer} value 
 * @param {Node} node
 * @returns {Boolean}
 */
const portWrite = function portWriteMessage(port, value, node) {
    port.write(value, function(err) {
        if (err) {
            node.error('Serial port write error : ', err.message);
            return false;
        }
    });
    return true;
}


/**
 * binary2ascii returns a ascii string converted from binary array.
 * 
 * @param {Buffer} data 
 * @returns {String} result
 */
const b2a = function binary2ascii(data) {
    let result = String();
    for(let i = 0; i < data.length; i++) {
        if (data[i] != 0x0A) {
            result = result + String.fromCharCode(data[i]);
        }
    }
    return result;
}

/**
 * decodeReqStatus() decode a response of status request.
 * 
 * @param {Buffer} response 
 * @param {Json} settings 
 * @returns {Boolean} result
 */
const decodeStatus = function decodeResponseStatusReq(response, settings) {
    if (response.length < 9) {
        return false;
    }
    result = b2a(response);
    if (result[1] & 2) {
        settings.modeEnvDiff = true;
    }
    else {
        settings.modeEnvDiff = false;
    }
    if (result[1] & 4) {
        settings.modeAutoEnvUpdate = true;
    }
    else {
        settings.modeAutoEnvUpdate = false;
    }
    settings.humanThreshold = (response[3] - "0".charCodeAt(0)) * 10 + (response[4] - "0".charCodeAt(0));
    settings.envThreshold = (response[6] - "0".charCodeAt(0)) * 10 + (response[7] - "0".charCodeAt(0));

    return true;
}

/**
 * decodeResponseHumanCount returns a json object of human count.
 * 
 * @param {Buffer} response 
 * @returns {Json} HumanCount
 */
const decodeHumanCount = function decodeResponseHumanCount(response) {
    if (response.length < 3) {
        return { 'humanCount': 'sensor data error' };
    }
    let arry = [];
    for(let i = 0; i < ((response.length - 3) / 2); i++) {
        arry.push(String.fromCharCode(response[i * 2]) + String.fromCharCode(response[i * 2 + 1]));
    }
    return { 'humanCount': {
                'count': (response[0] - "0".charCodeAt(0)) * 10 + (response[1] - "0".charCodeAt(0)),
                'elements': arry
                }
            };
}